---
title:Installing ArchEasy
author:Techmmr
date:16/09/2023
---
<img src="https://gitlab.com/techmmryt/archeasy/-/raw/main/Docs/Logo.png" alt="Created using the Arch Linux oficial Logo"/>
  
# Index 

1. [First Form to Install: instaling the script in the offcial Arch Linux Iso (now the only form to use the scripts)](#installing-1-using-the-official-iso)
2. [Second Form to Install: downloading the modified iso with my tools](#installing-2-using-the-modified-iso-with-my-tools)
3. [How to use balena Etcher to flash the Iso](#how-to-use-balena-etcher-to-flash-the-iso)
4. [How to boot the Usb](#how-to-boot-with-usb)
5. [About us and more](#about-us-and-more)
  
  
  
  
# Installing 1: Using the official Iso

## First Part
First, you have to download the iso image from the official website (www.archlinux.org)

[comment]: <> (Here is an image of archlinux website)
<img src="https://gitlab.com/techmmryt/archeasy/-/raw/main/Docs/Arch%20Linux%20Website%20Capture.png" alt="Extracted from Arch Linux Official Website" />

When you are in the website, you have to go to download apart.

[comment]: <> (An image of archlinux website with a line highlighting the downloads apart)
<img src="https://gitlab.com/techmmryt/archeasy/-/raw/main/Docs/Arch%20Linux%20Website%20Downloads%20Capture.png" alt="Extracted from Arch Linux Official Website" />

Now, you have to go down an search your country

[comment]: <> (Put an image from the diferent contries)
<img src="https://gitlab.com/techmmryt/archeasy/-/raw/main/Docs/Arch%20Linux%20Website%20Download%20Servers%20Capture.png" alt="Extracted from Arch Linux Official Website" />

Select a server

[comment]: <> (An image of one country servers with a line highlighting one of the servers)
<img src="https://gitlab.com/techmmryt/archeasy/-/raw/main/Docs/Arch%20Linux%20Website%20Download%20United%20Kingdom%20Severs%20Capture.png" alt="Extracted from Arch Linux Official Website" />

Now you are in a diferent website, select the firts file how has finaly a .iso

The download started, wait a few minutes, it's depends of your internet!!

## Second Part: Preparing the Boot Usb and Boot Arch Linux

The steps to flash the iso are in other part of this document. To go, [click here](#how-to-use-balena-etcher-to-flash-the-iso)

## Third Parte: Boot the Usb

The steps to boot with the USB are in other part of this document. To go, [click here](#how-to-boot-with-usb)


## Fourth Part: Commands to install Scripts in Iso console:

When the USB finishes booting and you get this screen:

[comment]: <> (An image of Boot Arch Linux Console)

You have to put this commmand to change the keyboard distribution:

~~~
loadkeys es [For Spanish Keyboard]
loadkeys uk [For English Keyboard]
~~~

Install __Git__ for clone the files with this command:

~~~
pacman -Sy git
~~~

Clone the repository

~~~
git clone https://gitlab.com/techmmryt/archeasy
~~~

Go to the Scripts Route

~~~
cd ./archeasy
~~~

Open the script 

~~~
bash main.sh
~~~

Continue the steps for finish the __Arch Linux__ Install
  
If you need help, you can view the __HTML__ file in the repository or use the __HELP__ integred in the scripts.

# Installing 2: Using the modified iso with my tools
  
## First Part

Go to the this repository [Tools for linux](https://gitlab.com/techmmryt/toolsforlinux "Tools for linux"), and download your favourite Iso version.

[comment]: <> (An image of Repository)

## Second Part

When the iso is downloaded, we will use balena etcher to flash it.The steps are in other part of this document. To go, [click here](#how-to-use-balena-etcher-to-flash-the-iso)

## Third Part

The steps to boot with the USB are in other part of this document. To go, [click here](#how-to-boot-with-usb)

## Fourth Part

Now you are in a terminal, like this:

[comment]: <> (An image of Iso terminal)

To open the Script write:

~~~
cd ./Scripts/archeasy/"modified iso"/

bash archeasy.sh
~~~

Continue the steps for finish the __Arch Linux__ Install
  
If you need help, you can view the __HTML__ file in the repository or use the __HELP__ integred in the scripts.

# How to use balena etcher to flash the Iso
## 1.Download the program 
Go to oficial [website](https://etcher.balena.io/)


Press download


And the program begins to download


When it finish, open the .exe and install the app


Follow the steps to install the app.


## 2.Create the Boot Usb

When it finish, open the program. You have an interface like that:


Press "Select Image"

A new screen will be apered to select the Iso. Search the directory and select the Iso. Later press "OK" to select that.

Do the same as with "Select Device"

When you finish, press "flash". Then, a new screen will be apered asking the administror permisions or the password.If is Admin Permsions, Pulse "ok". If is asking the password, put the password.

After this steep, automaticly, the program begins to prepare the usb.

Wait for the following screen to appear:

Now the usb is prepare to install Arch Linux.
1. [Return to Install 1](#third-parte-boot-the-usb)
2. [Return to Install 2](#third-part)

# How to boot with USB

Reboot your computer, and depending your computer press a key:

---
ASUS: F2
HP: Esc
Lenovo:
Dell: F2 // Fn + F2
Acer: F2
Msi:
Gigabyte:
Compaq:
Razer:
Toshiba:
Samsung:
Fujitsu:
Sony:
---

If is your bios is similar of that:


You have a BIOS, for configure [press here](#Bios-setting)

If you have that:


You have a UEFI, for configure [press here](#UEFI-setting)

## Bios setting

## UEFI setting