# !/bin/bash

clear

echo "Welcome to Arch Easy Iso Script"

# Here is an image of ArchLinuxEasyIso logo

echo "It's almost a exact copy of the official code of main.sh"
echo "This part is the only diference between this script and main.sh"

while ["$internetiso" != "S"]
do
    echo -e "Do you have the computer connected to the internet with Ethernet and you have downloaded the Internet Iso ? [S],[N]: "
    read internetiso

    if ["$internetiso" == "S"];
    then 
        cd ./"Internet Iso Files"
        bash repositorycloning.sh
    elif ["$intenetiso" == "N"];
    then
        cd ../
        bash main.sh 
    else
        echo "Your answer isn't correct"
    fi
done        