---
title:ArchEasy
author:Techmmr
date:07/09/2023
---

# Expain

This proyect, makes the Arch Linux installing easy with the __Bash Scripts__ (Performing the steps automatically with some questions previously to personalize the installation and choose certain necessary parameters), and the intuitive interfaze to make the best experience for the user.  

This proyect is made the most part with Bash and one part are Python scripts.

This proyect only are support for two languajes: __Spanish__ and __English__.  

The script also works in Virtual Machine (Virtual Box, VMware,...).  

There is a script call __HELP.py__, who gives you help if you don't understand one part, command, step, etc..  
