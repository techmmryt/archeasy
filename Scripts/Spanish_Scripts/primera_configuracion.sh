# !/bin/bash


clear

echo "El siguiente paso es configurar el Internet"

while [ "$conexioninternet" != "S" ] && [ "$conexioninternet" != "N" ]
do
    clear
    echo -e "¿Tienes tu ordenador conectado a internet por cable de Ethernet (Recomendado) Sí [S] o No [N] \n"
    read conexioninternet

    if [ "$conexioninternet" == "N" ];
    then 
        echo "Has contestado que no lo tienes conectado por Ethernet"

    elif [ "$conexionainternet" == "S" ];
    then
        echo -e "Has contestado que tienes el ordenador conectado a internet por cable de Ethernet\n"
        clear
        echo -e "Vamos a hacer una prueba de internet, cuando tengas más de 5 lineas, pulsa ctrl + c\n"
        ping 9.9.9.9
        echo "Si no tienes ningún error y todos los paquetes se han enviado/recibido correctamente es porque tu internet es estable"

        while ["$estadointernet" != "C"] && ["$estadointernet" != "NC"]
        do 
            clear 
            echo -e "¿Tu internet es estable [C] o no es estable [NC]?: "
            read estadointernet

            if [ "$estadointernet" == "C" ];
            then 
                echo "Tu internet es estable"
                echo "Ahora vamos a configurar el particionado de disco"
                timedatectl set-ntp true
                bash diskpartitioning.sh

            elif [ "$estadointernet" == "NC" ];
            then
                echo "Comprueba el cable de ethernet cable y prueba de nuevo"
            else
                echo "Tu respuesta no es correcta, inténtalo de nuevo!"    
            fi    
    else
        clear
        echo "Tu respuesta no es correcta, inténtalo de nuevo!"
    fi
done  
