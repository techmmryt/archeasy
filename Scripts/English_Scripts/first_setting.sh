# !/bin/bash

clear

# In this part you configure the keyboard
while [ "$keyboarddistribution" != "US" ] && [ "$keyboarddistribution" != "UK" ]
do
    echo -e "What's your keyboard distribution? United States [US] or United Kingdom [UK]: \n"
    read keyboarddistribution

    if [ "$keyboarddistribution" == "UK" ];
    then 
        echo "You select United Kingdom [UK] keyboard distribution"

    elif [ "$keyboarddistribution" == "US" ];
    then
        echo "You select United States [US] keyboard distribution"
    else
        clear
        echo "Your answer is incorrect, try again! "
    fi
done   

echo "The next step is configure the internet"

while [ "$internetconnection" != "S" ] && [ "$internetconnection" != "N" ]
do
    clear
    echo -e "Do you have your computer connect with a ethernet cable (Recomend) [S], or no [N] \n"
    read internetconnection

    if [ "$internetconnection" == "N" ];
    then 
        echo "You answered that you haven't it connected by ethernet cable or you don't have the posibility"

    elif [ "$internetconnection" == "S" ];
    then
        echo -e "You answered that you have it connected by ethernet cable\n"
        clear
        echo -e "When you have more than 5 lines of ping, int ctrl + C\n"
        ping 9.9.9.9
        echo "If you don't have any error and all packets were send correctly, your internet is stable"

        while ["$internetstatus" != "C"] && ["$internetstatus" != "NC"]
        do 
            clear 
            echo -e "Your Internet is Correct [C] or not have stable conexion [NC]: "
            read internetstatus

            if [ "$internetstatus" == "C" ];
            then 
                echo "Your internet is stable"
                echo "Now we have to configure the disk partitioning"
                timedatectl set-ntp true
                bash diskpartitioning.sh

            elif [ "$internetconnection" == "NC" ];
            then
                echo "Check the ethernet cable and try again"
            else
                echo "Your answer isn't correct, try again!"  
            fi      
    else
        clear
        echo "Your answer isn't correct, try again!!"
    fi
done   